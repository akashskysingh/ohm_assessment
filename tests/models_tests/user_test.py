from tests import OhmTestCase

from models import User, RelUser, RelUserMulti


class UserTest(OhmTestCase):

    def test_get_multi(self):
        assert self.chuck.get_multi("PHONE") == ['+14086441234', '+14086445678']
        assert self.justin.get_multi("PHONE") == []

    def test_get_location_by_id(self):
        user = User.query.get(1)
        location = User.get_location_by_id(user.user_id)
        found_ref = RelUser.query.filter(RelUser.user_id == user.user_id)
        assert found_ref[0].attribute == location[0]


    def test_phone_numbers_by_id(self):
        user = User.query.get(1)
        phone = User.get_phone_numbers_by_id(user.user_id)
        found_ref = RelUserMulti.query.filter(RelUserMulti.user_id == user.user_id)
        assert found_ref[0].attribute == phone[0]
