from app_main import app
from tests import OhmTestCase

from models import User


class CommunityTest(OhmTestCase):

    def test_presence_of_table(self):
        users = User.query.order_by(User.create_time.desc()).limit(5).all()

        with app.test_client() as c:
            response = c.get('/community')

            for user in users:
                assert user.full_name() in response.data
                assert user.tier in response.data
                assert str(user.point_balance) in response.data
                assert User.get_location_by_id(user.user_id)[0] in response.data
                phones = User.get_phone_numbers_by_id(user.user_id)

                for phone in phones:
                    assert phone in response.data
