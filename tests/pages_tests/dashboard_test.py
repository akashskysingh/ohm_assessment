from app_main import app
from tests import OhmTestCase

from flask.ext.login import current_user, login_user

class DashboardTest(OhmTestCase):
    def test_get(self):
        with app.test_client() as c:
            response = c.get('/dashboard')
            assert "Ready to begin assessment" in response.data

    def email_get(self):
        with app.test_client() as c:
            response = c.get('/dashboard')
            login_user(User.query.get(1))
            assert current_user.email_address in response.data

    def point_balance_get(self):
        with app.test_client() as c:
            response = c.get('/dashboard')
            login_user(User.query.get(1))
            assert current_user.point_balance in response.data

    def migration_upgrade_check(self):
        user1 = User.query.get(1)
        assert user1.point_balance == 1000
        location2 = RelUser.query.filter(RelUser.user_id == 2)
        assert location2.attribute == "USA"
        user3 = User.query.get(3)
        assert user3.tier == "Silver"

    def community_option_get(self):
        with app.test_client() as c:
            response = c.get('/dashboard')
            login_user(User.query.get(1))
            assert "community" in response.data
