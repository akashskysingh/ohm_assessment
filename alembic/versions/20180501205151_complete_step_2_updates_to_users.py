"""complete step 2 updates to users

Revision ID: 53a88518caf7
Revises: 00000000
Create Date: 2018-05-01 20:51:51.372414

"""

# revision identifiers, used by Alembic.
revision = '53a88518caf7'
down_revision = '00000000'

from alembic import op
import sqlalchemy as sa

connection = op.get_bind()

user = sa.sql.table('user',
    sa.column('user_id', sa.Integer),
    sa.column('point_balance', sa.Integer),
    sa.column('tier', sa.String)
)

rel_user = sa.sql.table('rel_user',
    sa.column('user_id', sa.Integer),
    sa.column('rel_lookup', sa.String),
    sa.column('attribute', sa.String)

)


def upgrade():
    # pass
    user1_up = user.update().\
        where(user.c.user_id == op.inline_literal(1)).\
        values({'point_balance':op.inline_literal(1000)})

    connection.execute(user1_up)

    user2_up = rel_user.insert().\
        values({
                'user_id': op.inline_literal(2),
                'rel_lookup': op.inline_literal('LOCATION'),
                'attribute': op.inline_literal('USA')
            })
    connection.execute(user2_up)

    user3_up = user.update().\
        where(user.c.user_id == op.inline_literal(3)).\
        values({'tier': op.inline_literal('Silver')})

    connection.execute(user3_up)




def downgrade():
    # pass
    user1_down = user.update().\
        where(user.c.user_id==op.inline_literal(1)).\
        values({'point_balance':op.inline_literal(0)})

    connection.execute(user1_down)

    user2_down = rel_user.delete().\
        where(
            # rel_user.c.user_id == op.inline_literal(2)
            sa.and_(
                rel_user.c.user_id == 2,
                rel_user.c.rel_lookup == 'LOCATION',
                rel_user.c.attribute == 'USA'
            )
        )

    connection.execute(user2_down)

    user3_down = user.update().\
        where(user.c.user_id == op.inline_literal(3)).\
        values({'tier':op.inline_literal('Carbon')})

    connection.execute(user3_down)
