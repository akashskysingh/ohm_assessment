from flask import jsonify, Response, request
from flask.ext.login import current_user

from functions import app
from models import User, RelUserMulti, RelUser

import datetime

def create_header_list_item():
        list_item = "<li class='community_list_item_header community_list_item'>"
        list_item_end = "</li>"

        list_item_name = "<div class='comm_user_name'>User Name:</div>"
        list_item_tier = "<div class='comm_user_tier'>Tier:</div>"
        list_item_pt_bal = "<div class='comm_user_pt_bal'>Points:</div>"
        list_item_phn_num = "<div class='comm_user_p_num'>Phone Number(s):</div>"

        list_item += (list_item_name + list_item_tier + list_item_pt_bal + list_item_phn_num + list_item_end)

        return list_item

# Create seperate item html elements to allow for customized styling
def create_user_list_item(user):
    list_item = "<li class='community_list_item'>"
    li_end = "</li>"
    d_end = "</div>"

    li_core = "<div class='comm_user_core'>"

    loc = User.get_location_by_id(user.user_id)[0]
    data_loc = "data-location=" + "'" + loc + "'"

    li_name = "<div class='js-comm_user_name comm_user_name'" + data_loc + ">" + user.full_name() +d_end
    li_tier = "<div class='comm_user_tier'>" + user.tier + d_end
    li_pt_bal = "<div class='comm_user_pt_bal'>" + str(user.point_balance) + d_end

    phn_nums = User.get_phone_numbers_by_id(user.user_id)
    li_phn_num = "<div class='comm_user_p_num'>" + phn_nums[0] + d_end

    list_item += (li_core + li_name + li_tier + li_pt_bal + li_phn_num + d_end)

    inc = 1
    while inc <= len(phn_nums) - 1:
        add_li_phn_num = "<div class='comm_user_p_num comm_user_p_num_add'>" + phn_nums[inc] + d_end
        list_item += add_li_phn_num
        inc += 1

    return list_item + li_end

@app.route('/community', methods=['GET'])
def community():
    users = User.query.order_by(User.create_time.desc()).limit(5).all()
    u_list = "<ul class='community_list'>" + create_header_list_item()
    ul_end = "</ul>"
    for user in users:
        u_list += create_user_list_item(user)
    msg = u_list + ul_end
    return jsonify(msg = msg)
